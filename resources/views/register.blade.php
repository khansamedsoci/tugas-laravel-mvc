<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Form Page</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
    @csrf
      <label for="fname">First name:</label><br /><br />
      <input type="text" id="fname" name="fname" /><br /><br />
      <label for="lname">Last name:</label><br /><br />
      <input type="text" id="lname" name="lname" /><br /><br />

      <label for="gender">Gender:</label> <br /><br />
      <input type="radio" id="male" name="gender" value="male" />
      <label for="male">Male</label><br />
      <input type="radio" id="female" name="gender" value="female" />
      <label for="female">Female</label><br />
      <input type="radio" id="other" name="gender" value="other" />
      <label for="other">Other</label><br /><br />

      <label for="nationality">Nationality:</label> <br /><br />
      <select name="nationality" id="nationality">
        <option value="indonesian">Indonesian</option>
        <option value="british">British</option>
        <option value="australians">Australians</option>
        <option value="otherNat">Other</option>
      </select>
      <br />

      <p>Language Spoken:</p>
      <input type="checkbox" id="indonesia" name="language" value="indonesia" />
      <label for="male">Indonesia</label><br />
      <input type="checkbox" id="english" name="language" value="english" />
      <label for="female">English</label><br />
      <input type="checkbox" id="otherLang" name="language" value="otherLang" />
      <label for="other">Other</label><br /><br />

      <label for="bio">Bio:</label><br /><br />
      <textarea id="bio" name="bio" rows="10" cols="50"></textarea><br />

      <input type="submit" value="Sign Up" />
    </form>
  </body>
</html>
