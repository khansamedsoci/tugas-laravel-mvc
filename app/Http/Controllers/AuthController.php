<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function form(){
        return view('register');
    }

    public function welcome() {
        return view('welcome');
    }

    public function welcome_post(Request $request) {
        // $request["fname","lname"]
        $fname = $request["fname"];
        $lname = $request["lname"];
        // return $fname . " " . $lname;
        return view('welcome', ["fname"=> $fname, "lname"=>$lname]);
    }
}
